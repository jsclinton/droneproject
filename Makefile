# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

# Default target executed when no arguments are given to make.
default_target: all
.PHONY : default_target

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/ardrone/ros_workspace/sandbox/drone_project

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/ardrone/ros_workspace/sandbox/drone_project

#=============================================================================
# Targets provided globally by CMake.

# Special rule for the target edit_cache
edit_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running interactive CMake command-line interface..."
	/usr/bin/cmake -i .
.PHONY : edit_cache

# Special rule for the target edit_cache
edit_cache/fast: edit_cache
.PHONY : edit_cache/fast

# Special rule for the target rebuild_cache
rebuild_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake to regenerate build system..."
	/usr/bin/cmake -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : rebuild_cache

# Special rule for the target rebuild_cache
rebuild_cache/fast: rebuild_cache
.PHONY : rebuild_cache/fast

# The main all target
all: cmake_check_build_system
	$(CMAKE_COMMAND) -E cmake_progress_start /home/ardrone/ros_workspace/sandbox/drone_project/CMakeFiles /home/ardrone/ros_workspace/sandbox/drone_project/CMakeFiles/progress.marks
	$(MAKE) -f CMakeFiles/Makefile2 all
	$(CMAKE_COMMAND) -E cmake_progress_start /home/ardrone/ros_workspace/sandbox/drone_project/CMakeFiles 0
.PHONY : all

# The main clean target
clean:
	$(MAKE) -f CMakeFiles/Makefile2 clean
.PHONY : clean

# The main clean target
clean/fast: clean
.PHONY : clean/fast

# Prepare targets for installation.
preinstall: all
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall

# Prepare targets for installation.
preinstall/fast:
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall/fast

# clear depends
depend:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 1
.PHONY : depend

#=============================================================================
# Target rules for targets named DroneProject

# Build rule for target.
DroneProject: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 DroneProject
.PHONY : DroneProject

# fast build rule for target.
DroneProject/fast:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/build
.PHONY : DroneProject/fast

#=============================================================================
# Target rules for targets named ROSBUILD_genmsg_cpp

# Build rule for target.
ROSBUILD_genmsg_cpp: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 ROSBUILD_genmsg_cpp
.PHONY : ROSBUILD_genmsg_cpp

# fast build rule for target.
ROSBUILD_genmsg_cpp/fast:
	$(MAKE) -f CMakeFiles/ROSBUILD_genmsg_cpp.dir/build.make CMakeFiles/ROSBUILD_genmsg_cpp.dir/build
.PHONY : ROSBUILD_genmsg_cpp/fast

#=============================================================================
# Target rules for targets named ROSBUILD_genmsg_lisp

# Build rule for target.
ROSBUILD_genmsg_lisp: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 ROSBUILD_genmsg_lisp
.PHONY : ROSBUILD_genmsg_lisp

# fast build rule for target.
ROSBUILD_genmsg_lisp/fast:
	$(MAKE) -f CMakeFiles/ROSBUILD_genmsg_lisp.dir/build.make CMakeFiles/ROSBUILD_genmsg_lisp.dir/build
.PHONY : ROSBUILD_genmsg_lisp/fast

#=============================================================================
# Target rules for targets named ROSBUILD_gensrv_cpp

# Build rule for target.
ROSBUILD_gensrv_cpp: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 ROSBUILD_gensrv_cpp
.PHONY : ROSBUILD_gensrv_cpp

# fast build rule for target.
ROSBUILD_gensrv_cpp/fast:
	$(MAKE) -f CMakeFiles/ROSBUILD_gensrv_cpp.dir/build.make CMakeFiles/ROSBUILD_gensrv_cpp.dir/build
.PHONY : ROSBUILD_gensrv_cpp/fast

#=============================================================================
# Target rules for targets named ROSBUILD_gensrv_lisp

# Build rule for target.
ROSBUILD_gensrv_lisp: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 ROSBUILD_gensrv_lisp
.PHONY : ROSBUILD_gensrv_lisp

# fast build rule for target.
ROSBUILD_gensrv_lisp/fast:
	$(MAKE) -f CMakeFiles/ROSBUILD_gensrv_lisp.dir/build.make CMakeFiles/ROSBUILD_gensrv_lisp.dir/build
.PHONY : ROSBUILD_gensrv_lisp/fast

#=============================================================================
# Target rules for targets named clean-test-results

# Build rule for target.
clean-test-results: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 clean-test-results
.PHONY : clean-test-results

# fast build rule for target.
clean-test-results/fast:
	$(MAKE) -f CMakeFiles/clean-test-results.dir/build.make CMakeFiles/clean-test-results.dir/build
.PHONY : clean-test-results/fast

#=============================================================================
# Target rules for targets named rosbuild_precompile

# Build rule for target.
rosbuild_precompile: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 rosbuild_precompile
.PHONY : rosbuild_precompile

# fast build rule for target.
rosbuild_precompile/fast:
	$(MAKE) -f CMakeFiles/rosbuild_precompile.dir/build.make CMakeFiles/rosbuild_precompile.dir/build
.PHONY : rosbuild_precompile/fast

#=============================================================================
# Target rules for targets named rosbuild_premsgsrvgen

# Build rule for target.
rosbuild_premsgsrvgen: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 rosbuild_premsgsrvgen
.PHONY : rosbuild_premsgsrvgen

# fast build rule for target.
rosbuild_premsgsrvgen/fast:
	$(MAKE) -f CMakeFiles/rosbuild_premsgsrvgen.dir/build.make CMakeFiles/rosbuild_premsgsrvgen.dir/build
.PHONY : rosbuild_premsgsrvgen/fast

#=============================================================================
# Target rules for targets named rospack_genmsg

# Build rule for target.
rospack_genmsg: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 rospack_genmsg
.PHONY : rospack_genmsg

# fast build rule for target.
rospack_genmsg/fast:
	$(MAKE) -f CMakeFiles/rospack_genmsg.dir/build.make CMakeFiles/rospack_genmsg.dir/build
.PHONY : rospack_genmsg/fast

#=============================================================================
# Target rules for targets named rospack_genmsg_libexe

# Build rule for target.
rospack_genmsg_libexe: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 rospack_genmsg_libexe
.PHONY : rospack_genmsg_libexe

# fast build rule for target.
rospack_genmsg_libexe/fast:
	$(MAKE) -f CMakeFiles/rospack_genmsg_libexe.dir/build.make CMakeFiles/rospack_genmsg_libexe.dir/build
.PHONY : rospack_genmsg_libexe/fast

#=============================================================================
# Target rules for targets named rospack_gensrv

# Build rule for target.
rospack_gensrv: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 rospack_gensrv
.PHONY : rospack_gensrv

# fast build rule for target.
rospack_gensrv/fast:
	$(MAKE) -f CMakeFiles/rospack_gensrv.dir/build.make CMakeFiles/rospack_gensrv.dir/build
.PHONY : rospack_gensrv/fast

#=============================================================================
# Target rules for targets named test

# Build rule for target.
test: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 test
.PHONY : test

# fast build rule for target.
test/fast:
	$(MAKE) -f CMakeFiles/test.dir/build.make CMakeFiles/test.dir/build
.PHONY : test/fast

#=============================================================================
# Target rules for targets named test-future

# Build rule for target.
test-future: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 test-future
.PHONY : test-future

# fast build rule for target.
test-future/fast:
	$(MAKE) -f CMakeFiles/test-future.dir/build.make CMakeFiles/test-future.dir/build
.PHONY : test-future/fast

#=============================================================================
# Target rules for targets named test-results

# Build rule for target.
test-results: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 test-results
.PHONY : test-results

# fast build rule for target.
test-results/fast:
	$(MAKE) -f CMakeFiles/test-results.dir/build.make CMakeFiles/test-results.dir/build
.PHONY : test-results/fast

#=============================================================================
# Target rules for targets named test-results-run

# Build rule for target.
test-results-run: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 test-results-run
.PHONY : test-results-run

# fast build rule for target.
test-results-run/fast:
	$(MAKE) -f CMakeFiles/test-results-run.dir/build.make CMakeFiles/test-results-run.dir/build
.PHONY : test-results-run/fast

#=============================================================================
# Target rules for targets named tests

# Build rule for target.
tests: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 tests
.PHONY : tests

# fast build rule for target.
tests/fast:
	$(MAKE) -f CMakeFiles/tests.dir/build.make CMakeFiles/tests.dir/build
.PHONY : tests/fast

# target to build an object file
src/CameraImageProcess.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/CameraImageProcess.o
.PHONY : src/CameraImageProcess.o

# target to preprocess a source file
src/CameraImageProcess.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/CameraImageProcess.i
.PHONY : src/CameraImageProcess.i

# target to generate assembly for a file
src/CameraImageProcess.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/CameraImageProcess.s
.PHONY : src/CameraImageProcess.s

# target to build an object file
src/Flight.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/Flight.o
.PHONY : src/Flight.o

# target to preprocess a source file
src/Flight.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/Flight.i
.PHONY : src/Flight.i

# target to generate assembly for a file
src/Flight.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/Flight.s
.PHONY : src/Flight.s

# target to build an object file
src/ImageProcess.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/ImageProcess.o
.PHONY : src/ImageProcess.o

# target to preprocess a source file
src/ImageProcess.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/ImageProcess.i
.PHONY : src/ImageProcess.i

# target to generate assembly for a file
src/ImageProcess.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/ImageProcess.s
.PHONY : src/ImageProcess.s

# target to build an object file
src/Main.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/Main.o
.PHONY : src/Main.o

# target to preprocess a source file
src/Main.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/Main.i
.PHONY : src/Main.i

# target to generate assembly for a file
src/Main.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/Main.s
.PHONY : src/Main.s

# target to build an object file
src/NavigationData.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/NavigationData.o
.PHONY : src/NavigationData.o

# target to preprocess a source file
src/NavigationData.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/NavigationData.i
.PHONY : src/NavigationData.i

# target to generate assembly for a file
src/NavigationData.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/NavigationData.s
.PHONY : src/NavigationData.s

# target to build an object file
src/Pid.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/Pid.o
.PHONY : src/Pid.o

# target to preprocess a source file
src/Pid.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/Pid.i
.PHONY : src/Pid.i

# target to generate assembly for a file
src/Pid.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/Pid.s
.PHONY : src/Pid.s

# target to build an object file
src/Selector.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/Selector.o
.PHONY : src/Selector.o

# target to preprocess a source file
src/Selector.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/Selector.i
.PHONY : src/Selector.i

# target to generate assembly for a file
src/Selector.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/Selector.s
.PHONY : src/Selector.s

# target to build an object file
src/State.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/State.o
.PHONY : src/State.o

# target to preprocess a source file
src/State.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/State.i
.PHONY : src/State.i

# target to generate assembly for a file
src/State.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/State.s
.PHONY : src/State.s

# target to build an object file
src/StateData.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/StateData.o
.PHONY : src/StateData.o

# target to preprocess a source file
src/StateData.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/StateData.i
.PHONY : src/StateData.i

# target to generate assembly for a file
src/StateData.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/StateData.s
.PHONY : src/StateData.s

# target to build an object file
src/VideoImageProcess.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/VideoImageProcess.o
.PHONY : src/VideoImageProcess.o

# target to preprocess a source file
src/VideoImageProcess.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/VideoImageProcess.i
.PHONY : src/VideoImageProcess.i

# target to generate assembly for a file
src/VideoImageProcess.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/VideoImageProcess.s
.PHONY : src/VideoImageProcess.s

# target to build an object file
src/mftracker/BB.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/mftracker/BB.o
.PHONY : src/mftracker/BB.o

# target to preprocess a source file
src/mftracker/BB.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/mftracker/BB.i
.PHONY : src/mftracker/BB.i

# target to generate assembly for a file
src/mftracker/BB.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/mftracker/BB.s
.PHONY : src/mftracker/BB.s

# target to build an object file
src/mftracker/BBPredict.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/mftracker/BBPredict.o
.PHONY : src/mftracker/BBPredict.o

# target to preprocess a source file
src/mftracker/BBPredict.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/mftracker/BBPredict.i
.PHONY : src/mftracker/BBPredict.i

# target to generate assembly for a file
src/mftracker/BBPredict.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/mftracker/BBPredict.s
.PHONY : src/mftracker/BBPredict.s

# target to build an object file
src/mftracker/FBTrack.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/mftracker/FBTrack.o
.PHONY : src/mftracker/FBTrack.o

# target to preprocess a source file
src/mftracker/FBTrack.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/mftracker/FBTrack.i
.PHONY : src/mftracker/FBTrack.i

# target to generate assembly for a file
src/mftracker/FBTrack.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/mftracker/FBTrack.s
.PHONY : src/mftracker/FBTrack.s

# target to build an object file
src/mftracker/Lk.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/mftracker/Lk.o
.PHONY : src/mftracker/Lk.o

# target to preprocess a source file
src/mftracker/Lk.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/mftracker/Lk.i
.PHONY : src/mftracker/Lk.i

# target to generate assembly for a file
src/mftracker/Lk.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/mftracker/Lk.s
.PHONY : src/mftracker/Lk.s

# target to build an object file
src/mftracker/Median.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/mftracker/Median.o
.PHONY : src/mftracker/Median.o

# target to preprocess a source file
src/mftracker/Median.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/mftracker/Median.i
.PHONY : src/mftracker/Median.i

# target to generate assembly for a file
src/mftracker/Median.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/mftracker/Median.s
.PHONY : src/mftracker/Median.s

# target to build an object file
src/tld/Clustering.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/Clustering.o
.PHONY : src/tld/Clustering.o

# target to preprocess a source file
src/tld/Clustering.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/Clustering.i
.PHONY : src/tld/Clustering.i

# target to generate assembly for a file
src/tld/Clustering.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/Clustering.s
.PHONY : src/tld/Clustering.s

# target to build an object file
src/tld/DetectionResult.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/DetectionResult.o
.PHONY : src/tld/DetectionResult.o

# target to preprocess a source file
src/tld/DetectionResult.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/DetectionResult.i
.PHONY : src/tld/DetectionResult.i

# target to generate assembly for a file
src/tld/DetectionResult.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/DetectionResult.s
.PHONY : src/tld/DetectionResult.s

# target to build an object file
src/tld/DetectorCascade.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/DetectorCascade.o
.PHONY : src/tld/DetectorCascade.o

# target to preprocess a source file
src/tld/DetectorCascade.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/DetectorCascade.i
.PHONY : src/tld/DetectorCascade.i

# target to generate assembly for a file
src/tld/DetectorCascade.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/DetectorCascade.s
.PHONY : src/tld/DetectorCascade.s

# target to build an object file
src/tld/EnsembleClassifier.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/EnsembleClassifier.o
.PHONY : src/tld/EnsembleClassifier.o

# target to preprocess a source file
src/tld/EnsembleClassifier.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/EnsembleClassifier.i
.PHONY : src/tld/EnsembleClassifier.i

# target to generate assembly for a file
src/tld/EnsembleClassifier.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/EnsembleClassifier.s
.PHONY : src/tld/EnsembleClassifier.s

# target to build an object file
src/tld/ForegroundDetector.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/ForegroundDetector.o
.PHONY : src/tld/ForegroundDetector.o

# target to preprocess a source file
src/tld/ForegroundDetector.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/ForegroundDetector.i
.PHONY : src/tld/ForegroundDetector.i

# target to generate assembly for a file
src/tld/ForegroundDetector.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/ForegroundDetector.s
.PHONY : src/tld/ForegroundDetector.s

# target to build an object file
src/tld/MedianFlowTracker.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/MedianFlowTracker.o
.PHONY : src/tld/MedianFlowTracker.o

# target to preprocess a source file
src/tld/MedianFlowTracker.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/MedianFlowTracker.i
.PHONY : src/tld/MedianFlowTracker.i

# target to generate assembly for a file
src/tld/MedianFlowTracker.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/MedianFlowTracker.s
.PHONY : src/tld/MedianFlowTracker.s

# target to build an object file
src/tld/NNClassifier.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/NNClassifier.o
.PHONY : src/tld/NNClassifier.o

# target to preprocess a source file
src/tld/NNClassifier.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/NNClassifier.i
.PHONY : src/tld/NNClassifier.i

# target to generate assembly for a file
src/tld/NNClassifier.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/NNClassifier.s
.PHONY : src/tld/NNClassifier.s

# target to build an object file
src/tld/TLD.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/TLD.o
.PHONY : src/tld/TLD.o

# target to preprocess a source file
src/tld/TLD.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/TLD.i
.PHONY : src/tld/TLD.i

# target to generate assembly for a file
src/tld/TLD.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/TLD.s
.PHONY : src/tld/TLD.s

# target to build an object file
src/tld/TLDUtil.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/TLDUtil.o
.PHONY : src/tld/TLDUtil.o

# target to preprocess a source file
src/tld/TLDUtil.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/TLDUtil.i
.PHONY : src/tld/TLDUtil.i

# target to generate assembly for a file
src/tld/TLDUtil.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/TLDUtil.s
.PHONY : src/tld/TLDUtil.s

# target to build an object file
src/tld/VarianceFilter.o:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/VarianceFilter.o
.PHONY : src/tld/VarianceFilter.o

# target to preprocess a source file
src/tld/VarianceFilter.i:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/VarianceFilter.i
.PHONY : src/tld/VarianceFilter.i

# target to generate assembly for a file
src/tld/VarianceFilter.s:
	$(MAKE) -f CMakeFiles/DroneProject.dir/build.make CMakeFiles/DroneProject.dir/src/tld/VarianceFilter.s
.PHONY : src/tld/VarianceFilter.s

# Help Target
help:
	@echo "The following are some of the valid targets for this Makefile:"
	@echo "... all (the default if no target is provided)"
	@echo "... clean"
	@echo "... depend"
	@echo "... DroneProject"
	@echo "... ROSBUILD_genmsg_cpp"
	@echo "... ROSBUILD_genmsg_lisp"
	@echo "... ROSBUILD_gensrv_cpp"
	@echo "... ROSBUILD_gensrv_lisp"
	@echo "... clean-test-results"
	@echo "... edit_cache"
	@echo "... rebuild_cache"
	@echo "... rosbuild_precompile"
	@echo "... rosbuild_premsgsrvgen"
	@echo "... rospack_genmsg"
	@echo "... rospack_genmsg_libexe"
	@echo "... rospack_gensrv"
	@echo "... test"
	@echo "... test-future"
	@echo "... test-results"
	@echo "... test-results-run"
	@echo "... tests"
	@echo "... src/CameraImageProcess.o"
	@echo "... src/CameraImageProcess.i"
	@echo "... src/CameraImageProcess.s"
	@echo "... src/Flight.o"
	@echo "... src/Flight.i"
	@echo "... src/Flight.s"
	@echo "... src/ImageProcess.o"
	@echo "... src/ImageProcess.i"
	@echo "... src/ImageProcess.s"
	@echo "... src/Main.o"
	@echo "... src/Main.i"
	@echo "... src/Main.s"
	@echo "... src/NavigationData.o"
	@echo "... src/NavigationData.i"
	@echo "... src/NavigationData.s"
	@echo "... src/Pid.o"
	@echo "... src/Pid.i"
	@echo "... src/Pid.s"
	@echo "... src/Selector.o"
	@echo "... src/Selector.i"
	@echo "... src/Selector.s"
	@echo "... src/State.o"
	@echo "... src/State.i"
	@echo "... src/State.s"
	@echo "... src/StateData.o"
	@echo "... src/StateData.i"
	@echo "... src/StateData.s"
	@echo "... src/VideoImageProcess.o"
	@echo "... src/VideoImageProcess.i"
	@echo "... src/VideoImageProcess.s"
	@echo "... src/mftracker/BB.o"
	@echo "... src/mftracker/BB.i"
	@echo "... src/mftracker/BB.s"
	@echo "... src/mftracker/BBPredict.o"
	@echo "... src/mftracker/BBPredict.i"
	@echo "... src/mftracker/BBPredict.s"
	@echo "... src/mftracker/FBTrack.o"
	@echo "... src/mftracker/FBTrack.i"
	@echo "... src/mftracker/FBTrack.s"
	@echo "... src/mftracker/Lk.o"
	@echo "... src/mftracker/Lk.i"
	@echo "... src/mftracker/Lk.s"
	@echo "... src/mftracker/Median.o"
	@echo "... src/mftracker/Median.i"
	@echo "... src/mftracker/Median.s"
	@echo "... src/tld/Clustering.o"
	@echo "... src/tld/Clustering.i"
	@echo "... src/tld/Clustering.s"
	@echo "... src/tld/DetectionResult.o"
	@echo "... src/tld/DetectionResult.i"
	@echo "... src/tld/DetectionResult.s"
	@echo "... src/tld/DetectorCascade.o"
	@echo "... src/tld/DetectorCascade.i"
	@echo "... src/tld/DetectorCascade.s"
	@echo "... src/tld/EnsembleClassifier.o"
	@echo "... src/tld/EnsembleClassifier.i"
	@echo "... src/tld/EnsembleClassifier.s"
	@echo "... src/tld/ForegroundDetector.o"
	@echo "... src/tld/ForegroundDetector.i"
	@echo "... src/tld/ForegroundDetector.s"
	@echo "... src/tld/MedianFlowTracker.o"
	@echo "... src/tld/MedianFlowTracker.i"
	@echo "... src/tld/MedianFlowTracker.s"
	@echo "... src/tld/NNClassifier.o"
	@echo "... src/tld/NNClassifier.i"
	@echo "... src/tld/NNClassifier.s"
	@echo "... src/tld/TLD.o"
	@echo "... src/tld/TLD.i"
	@echo "... src/tld/TLD.s"
	@echo "... src/tld/TLDUtil.o"
	@echo "... src/tld/TLDUtil.i"
	@echo "... src/tld/TLDUtil.s"
	@echo "... src/tld/VarianceFilter.o"
	@echo "... src/tld/VarianceFilter.i"
	@echo "... src/tld/VarianceFilter.s"
.PHONY : help



#=============================================================================
# Special targets to cleanup operation of make.

# Special rule to run CMake to check the build system integrity.
# No rule that depends on this can have commands that come from listfiles
# because they might be regenerated.
cmake_check_build_system:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 0
.PHONY : cmake_check_build_system

